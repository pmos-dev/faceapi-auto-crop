import '@tensorflow/tfjs-node';

import * as canvas from 'canvas';
import * as faceapi from 'face-api.js';

import { commonsNumberMedian } from 'tscommons-es-core';
import { TXy } from 'tscommons-es-graphics';

import { commonsOutputDebug } from 'nodecommons-es-cli';

import { TDetection } from '../types/tface-detection';
import { TLandmarks } from '../types/tlandmarks';

import { EFaceApiModel } from '../enums/eface-api-model';

const { Canvas, Image, ImageData } = canvas;
// @ts-expect-error Copied from face-api.js instructions
faceapi.env.monkeyPatch({ Canvas, Image, ImageData });	// eslint-disable-line object-shorthand

type TCanvasModule = {
		loadImage: (src: string|Buffer, options?: { [ key: string ]: unknown }) => Promise<canvas.Image>;
		createCanvas: (width: number, height: number, type?: 'pdf'|'svg') => canvas.Canvas;
};

const canvasModule: TCanvasModule = canvas['default'] as TCanvasModule;

function getAverageLandmarkPoint(points: faceapi.Point[]): TXy|undefined {
	if (points.length === 0) return undefined;

	const xs: number[] = points
			.map((point: faceapi.Point): number => point.x);
	const ys: number[] = points
			.map((point: faceapi.Point): number => point.y);

	return {
			x: commonsNumberMedian(xs)!,
			y: commonsNumberMedian(ys)!
	};
}

function getFaceLandmarks(landmarks: faceapi.FaceLandmarks68): TLandmarks {
	return {
			leftEye: getAverageLandmarkPoint(landmarks.getLeftEye()),
			rightEye: getAverageLandmarkPoint(landmarks.getRightEye()),
			nose: getAverageLandmarkPoint(landmarks.getNose()),
			mouth: getAverageLandmarkPoint(landmarks.getMouth())
	};
}

export class FaceDetector {
	private options: faceapi.FaceDetectionOptions;

	constructor(
			private model: EFaceApiModel,
			options?: faceapi.FaceDetectionOptions
	) {
		// the faceapi.detectAllFaces uses the options to select which model, so we have to create one if none is supplied

		if (options) {
			this.options = options;
		} else {
			switch (this.model) {
				case EFaceApiModel.SSDV1:
					this.options = new faceapi.SsdMobilenetv1Options();
					break;
				case EFaceApiModel.TINY:
					this.options = new faceapi.TinyFaceDetectorOptions();
					break;
				case EFaceApiModel.MTCNN:
					this.options = new faceapi.MtcnnOptions();
					break;
			}
		}
	}

	public async init(modelPath = './models'): Promise<void> {
		switch (this.model) {
			case EFaceApiModel.SSDV1:
				await faceapi.nets.ssdMobilenetv1.loadFromDisk(modelPath);
				await faceapi.nets.faceLandmark68Net.loadFromDisk(modelPath);
				break;
			case EFaceApiModel.TINY:
				await faceapi.nets.tinyFaceDetector.loadFromDisk(modelPath);
				break;
			case EFaceApiModel.MTCNN:
				await faceapi.nets.mtcnn.loadFromDisk(modelPath);
				break;
		}
	}

	public async detect(
			buffer: Buffer
	): Promise<TDetection[]> {
		const image: canvas.Image = await canvasModule.loadImage(buffer);
		const c: canvas.Canvas = canvasModule.createCanvas(image.width, image.height);

		// @ts-expect-error Seems to be a missing export
		const ctx: canvas.NodeCanvasRenderingContext2D = c.getContext('2d');

		// eslint-disable-next-line @typescript-eslint/no-unsafe-member-access,@typescript-eslint/no-unsafe-call
		ctx.drawImage(image, 0, 0, image.width, image.height);
		
		const detections: faceapi.WithFaceLandmarks<{ detection: faceapi.FaceDetection }, faceapi.FaceLandmarks68>[] = await faceapi.detectAllFaces(
				c as unknown as faceapi.TNetInput,
				this.options
		)
				.withFaceLandmarks();

		commonsOutputDebug(`Detected ${detections.length} faces`);

		const tds: TDetection[] = detections
				.map((detection: faceapi.WithFaceLandmarks<{ detection: faceapi.FaceDetection }, faceapi.FaceLandmarks68>): TDetection => ({
						x: detection.detection.box.x,
						y: detection.detection.box.y,
						width: detection.detection.box.width,
						height: detection.detection.box.height,
						score: detection.detection.score,
						landmarks: getFaceLandmarks(detection.landmarks)
				}));

		for (const detection of tds) {
			commonsOutputDebug(`${detection.x},${detection.y} ${detection.width}x${detection.height} [score=${detection.score}]`);
		}
		
		return tds;
	}
}
