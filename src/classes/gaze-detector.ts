import * as tf from '@tensorflow/tfjs-node';

import { commonsTypeIsArray } from 'tscommons-es-core';
import { TXy } from 'tscommons-es-graphics';

import { TLandmarks } from '../types/tlandmarks';

function convertLandmark(value: number): number {
	return Math.max(0, Math.min(value));
}

function convertLandmarks(landmarks: TLandmarks): number[] {
	return [
			convertLandmark(landmarks.leftEye!.x),
			convertLandmark(landmarks.leftEye!.y),
			convertLandmark(landmarks.rightEye!.x),
			convertLandmark(landmarks.rightEye!.y),
			convertLandmark(landmarks.nose!.x),
			convertLandmark(landmarks.nose!.y),
			convertLandmark(landmarks.mouth!.x),
			convertLandmark(landmarks.mouth!.y)
	];
}

function deconvertCoordinate(value: number): number {
	return (value * 2) - 1;
}

export class GazeDetector {
	protected model: tf.LayersModel|undefined;

	public async init(path: string = './gaze-model'): Promise<void> {
		this.model = await tf.loadLayersModel(`file://${path}/model.json`);
	}

	public predict(landmarks: TLandmarks): TXy {
		if (!this.model) throw new Error('Model has not been initialised');

		const xs: tf.Tensor2D = tf.tensor2d([ convertLandmarks(landmarks) ]);

		const prediction: tf.Tensor<tf.Rank>|tf.Tensor<tf.Rank>[] = this.model.predict(xs);
		const single: tf.Tensor<tf.Rank> = commonsTypeIsArray(prediction) ? prediction[0] : prediction;
		const synced: Float32Array = single.dataSync() as Float32Array;

		const deconverted: TXy = {
				x: deconvertCoordinate(synced[0]),
				y: deconvertCoordinate(synced[1])
		};

		return deconverted;
	}
}
