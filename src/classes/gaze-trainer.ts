import { TXy } from 'tscommons-es-graphics';

import { TDetection } from '../types/tface-detection';

import { Jimp } from './jimp';

export type TImageWithLandmarks = {
		jimp: Jimp;
		leftEye: TXy;
		rightEye: TXy;
		nose: TXy;
		mouth: TXy;
};

export class GazeTrainer {
	constructor(
			private jimp: Jimp,
			private detections: TDetection[]
	) {}

	public async extract(): Promise<TImageWithLandmarks[]> {
		const extracted: TImageWithLandmarks[] = [];

		for (const detection of this.detections) {
			const clone: Jimp = this.jimp.clone();

			if (detection.landmarks.leftEye) {
				await clone.drawDetectionBoundary(
						{
								x: detection.landmarks.leftEye.x - 5,
								y: detection.landmarks.leftEye.y - 5,
								width: 10,
								height: 10
						},
						0xffffffff
				);
			}
			if (detection.landmarks.rightEye) {
				await clone.drawDetectionBoundary(
						{
								x: detection.landmarks.rightEye.x - 5,
								y: detection.landmarks.rightEye.y - 5,
								width: 10,
								height: 10
						},
						0xffa000ff
				);
			}
			if (detection.landmarks.nose) {
				await clone.drawDetectionBoundary(
						{
								x: detection.landmarks.nose.x - 5,
								y: detection.landmarks.nose.y - 5,
								width: 10,
								height: 10
						},
						0xa0ff00ff
				);
			}
			if (detection.landmarks.mouth) {
				await clone.drawDetectionBoundary(
						{
								x: detection.landmarks.mouth.x - 5,
								y: detection.landmarks.mouth.y - 5,
								width: 10,
								height: 10
						},
						0x00a0ffff
				);
			}

			await clone.crop(
					detection.x,
					detection.y,
					detection.width,
					detection.height
			);

			if (!detection.landmarks.leftEye || !detection.landmarks.rightEye || !detection.landmarks.nose || !detection.landmarks.mouth) continue;

			extracted.push({
					jimp: clone,
					leftEye: {
							x: (detection.landmarks.leftEye.x - detection.x) / detection.width,
							y: (detection.landmarks.leftEye.y - detection.y) / detection.height
					},
					rightEye: {
							x: (detection.landmarks.rightEye.x - detection.x) / detection.width,
							y: (detection.landmarks.rightEye.y - detection.y) / detection.height
					},
					nose: {
							x: (detection.landmarks.nose.x - detection.x) / detection.width,
							y: (detection.landmarks.nose.y - detection.y) / detection.height
					},
					mouth: {
							x: (detection.landmarks.mouth.x - detection.x) / detection.width,
							y: (detection.landmarks.mouth.y - detection.y) / detection.height
					}
			});
		}

		return extracted;
	}
}
