import { IDimensions } from 'tscommons-es-graphics';

import { commonsOutputDebug } from 'nodecommons-es-cli';
import { CommonsJimp } from 'nodecommons-es-graphics';

import { getCombinedBoundary } from '../helpers/face-detection';

import { TDetection } from '../types/tface-detection';

type TDimensions = {
		width: number;
		height: number;
};

type TRatio = {
		width: number;
		height: number;
};

function getGrowth(
		faceSize: number,
		imageSize: number,
		minGrowthFaceRatio: number,
		minGrowthImageRatio: number
): number {
	const faceGrowth: number = faceSize * minGrowthFaceRatio;
	commonsOutputDebug(`Face growth is ${faceGrowth}`);

	const imageGrowth: number = imageSize * minGrowthImageRatio;
	commonsOutputDebug(`Image growth is ${imageGrowth}`);

	return Math.max(
			faceGrowth,
			imageGrowth
	);
}

function shrinkToContain(
		boundaryWidth: number,
		boundaryHeight: number,
		srcWidth: number,
		srcHeight: number
): TDimensions {
	const boundaryRatio: number = boundaryWidth / boundaryHeight;

	// rubbish way of doing it, but it works

	while (boundaryWidth > srcWidth || boundaryHeight > srcHeight) {
		boundaryWidth--;
		boundaryHeight = boundaryWidth / boundaryRatio;
	}

	return {
			width: boundaryWidth,
			height: boundaryHeight
	};
}

export class Jimp extends CommonsJimp {
	public clone(): Jimp {
		if (!this.image) throw new Error('Image not loaded');

		const clone: Jimp = new Jimp();
		clone.image = this.image.clone();

		return clone;
	}

	public fillArea(
			x: number,
			y: number,
			w: number,
			h: number,
			color: number
	): Promise<void> {
		if (!this.image) throw new Error('Image not loaded');

		if (x < 0) {
			w += x;
			x = 0;
		}
		if (y < 0) {
			h += y;
			y = 0;
		}
		if ((x + w) >= this.getWidth()) {
			w = this.getWidth() - x;
		}
		if ((y + h) >= this.getHeight()) {
			h = this.getHeight() - y;
		}

		return new Promise((resolve: () => void, reject: (e: Error) => void): void => {
			this.image!
					.scan(
							x,
							y,
							w,
							h,
							(
									_x: number,
									_y: number,
									offset: number
							): void => {
								try {
									this.image!.bitmap.data.writeUInt32BE(color, offset);
								} catch (e) { /* do nothing */ }
							},
							(err: Error|null, _jimp: unknown): void => {
								if (err) return reject(err);
								resolve();
							}
					);
		});
	}

	public async drawDetectionBoundary(
			detection: IDimensions,
			color: number
	): Promise<void> {
		await this.fillArea(detection.x, detection.y, detection.width, 5, color);
		await this.fillArea(detection.x, detection.y, 5, detection.height, color);
		await this.fillArea(detection.x, detection.y + (detection.height - 5), detection.width, 5, color);
		await this.fillArea(detection.x + (detection.width - 5), detection.y, 5, detection.height, color);
	}

	public async cropAndResize(
			detections: TDetection[],
			destWidth: number,
			destHeight: number|'detection',
			minGrowthFaceRatio: TRatio,
			minGrowthImageRatio: TRatio,
			fullWidth: boolean = false,
			drawOutlines: boolean = false
	): Promise<void> {
		// in all of this, we always grow, never shrink

		const boundary: IDimensions = {
				x: 0,
				y: 0,
				width: this.getWidth(),
				height: this.getHeight()
		};

		if (detections.length > 0) {
			const growns: IDimensions[] = detections
					.map((detection: TDetection): IDimensions => {
						commonsOutputDebug('Computing width growth');
						const growthWidth: number = getGrowth(detection.width, this.getWidth(), minGrowthFaceRatio.width, minGrowthImageRatio.width);

						commonsOutputDebug('Computing height growth');
						const growthHeight: number = getGrowth(detection.height, this.getHeight(), minGrowthFaceRatio.height, minGrowthImageRatio.height);

						return {
								x: detection.x + (detection.width / 2) - (growthWidth / 2),
								y: detection.y + (detection.height / 2) - (growthHeight / 2),
								width: growthWidth,
								height: growthHeight
						};
					});
			
			for (const grown of growns) {
				commonsOutputDebug(`Grown detection is now ${grown.x},${grown.y} ${grown.width}x${grown.height}`);
			}

			const facialBoundary: IDimensions = getCombinedBoundary(growns);
			commonsOutputDebug(`Combined facial boundary is ${facialBoundary.x},${facialBoundary.y} ${facialBoundary.width}x${facialBoundary.height}`);

			if (facialBoundary.x < 0) facialBoundary.x = 0;
			if (facialBoundary.x >= this.getWidth()) facialBoundary.x = this.getWidth() - 1;
			if (facialBoundary.y < 0) facialBoundary.y = 0;
			if (facialBoundary.y >= this.getHeight()) facialBoundary.y = this.getHeight() - 1;
			if ((facialBoundary.x + facialBoundary.width) >= this.getWidth()) facialBoundary.width = (this.getWidth() - facialBoundary.x) - 1;
			if ((facialBoundary.y + facialBoundary.height) >= this.getHeight()) facialBoundary.height = (this.getHeight() - facialBoundary.y) - 1;

			commonsOutputDebug(`Clamped facial boundary is ${facialBoundary.x},${facialBoundary.y} ${facialBoundary.width}x${facialBoundary.height}`);

			boundary.x = facialBoundary.x;
			boundary.y = facialBoundary.y;
			boundary.width = facialBoundary.width;
			boundary.height = facialBoundary.height;

			if (drawOutlines) {
				await this.drawDetectionBoundary(boundary, 0x00ff00ff);
			}
		}

		commonsOutputDebug(`Cropping using boundary ${boundary.x},${boundary.y} ${boundary.width}x${boundary.height}`);

		let cx: number = boundary.x + (boundary.width / 2);
		const cy: number = boundary.y + (boundary.height / 2);

		if (fullWidth) {
			commonsOutputDebug('Full width requested, so fixed width to that');
			boundary.x = 0;
			boundary.width = this.getWidth();
			cx = this.getWidth() / 2;
		}

		let srcRatio: number = boundary.width / boundary.height;

		if (destHeight === 'detection') destHeight = destWidth / srcRatio;

		if (destWidth > boundary.width || destHeight > boundary.height) {
			commonsOutputDebug(`Destination size ${destWidth}x${destHeight} is larger than boundary size ${boundary.width}x${boundary.height}. Need to expand`);

			if (destWidth > boundary.width) boundary.width = destWidth;
			if (destHeight > boundary.height) boundary.height = destHeight;

			boundary.x = cx - (boundary.width / 2);
			boundary.y = cy - (boundary.height / 2);

			commonsOutputDebug(`Boundary is now ${boundary.x},${boundary.y} ${boundary.width}x${boundary.height}`);
			if (drawOutlines) {
				await this.drawDetectionBoundary(boundary, 0xff00ffff);
			}
		}

		const destRatio: number = destWidth / destHeight;

		commonsOutputDebug(`Src ratio is ${srcRatio}; dest ratio needs to be ${destRatio}`);

		// rubbish but works for now
		while (srcRatio < destRatio) {
			// not wide enough
			boundary.width++;
			srcRatio = boundary.width / boundary.height;
		}
		while (srcRatio > destRatio) {
			// not tall enough
			boundary.height++;
			srcRatio = boundary.width / boundary.height;
		}

		commonsOutputDebug(`After adjusting, src ratio is ${srcRatio}; dest ratio is ${destRatio}`);

		boundary.x = cx - (boundary.width / 2);
		boundary.y = cy - (boundary.height / 2);
		if (drawOutlines) {
			await this.drawDetectionBoundary(boundary, 0xffff00ff);
		}

		commonsOutputDebug(`Final pre-clamped boundary is ${boundary.x},${boundary.y} ${boundary.width}x${boundary.height}`);

		// clamp to the src width and height max
		if (boundary.width > this.getWidth() || boundary.height > this.getHeight()) {
			commonsOutputDebug(`Boundary size ${boundary.width}x${boundary.height} is larger than src image size ${this.getWidth()}x${this.getHeight()}. Need to shrink`);

			const shrunk: TDimensions = shrinkToContain(
					boundary.width,
					boundary.height,
					this.getWidth(),
					this.getHeight()
			);

			boundary.width = shrunk.width;
			boundary.height = shrunk.height;
			boundary.x = cx - (boundary.width / 2);
			boundary.y = cy - (boundary.height / 2);

			commonsOutputDebug(`Boundary is now ${boundary.x},${boundary.y} ${boundary.width}x${boundary.height}`);
		}

		// move down/right if offset negative
		while (boundary.x < 0) boundary.x++;
		while (boundary.y < 0) boundary.y++;

		// move up/left if offset over edge of right
		while ((boundary.width + boundary.x) > this.getWidth()) boundary.x--;
		while ((boundary.height + boundary.y) > this.getHeight()) boundary.y--;

		commonsOutputDebug(`Eventual boundary is ${boundary.x},${boundary.y} ${boundary.width}x${boundary.height}`);

		const sanitised: IDimensions = {
				x: Math.max(0, Math.round(boundary.x)),
				y: Math.max(0, Math.round(boundary.y)),
				width: Math.min(this.getWidth(), Math.round(boundary.width)),
				height: Math.min(this.getHeight(), Math.round(boundary.height))
		};
		if ((sanitised.x + sanitised.width) > this.getWidth()) sanitised.x--;
		if ((sanitised.y + sanitised.height) > this.getHeight()) sanitised.y--;
		if (sanitised.x < 0 || sanitised.y < 0) throw new Error('Unable to sanitise x, y, width and height. This should not be possible');

		commonsOutputDebug(`Final sanitised boundary for crop ${sanitised.x},${sanitised.y} ${sanitised.width}x${sanitised.height}`);

		await this.crop(
				sanitised.x,
				sanitised.y,
				sanitised.width,
				sanitised.height
		);
		await this.resize(destWidth, destHeight);
	}
}
