#!/bin/sh
":" //# comment; exec /usr/bin/env node --experimental-modules --experimental-specifier-resolution=node "$0" "$@"

import * as faceapi from 'face-api.js';

import { commonsBase62GenerateRandomId, commonsTypeAttemptNumber } from 'tscommons-es-core';
import { TXy } from 'tscommons-es-graphics';

import {
		commonsOutputDebug,
		commonsOutputDie,
		commonsOutputInfo,
		commonsOutputSetDebugging
} from 'nodecommons-es-cli';
import { CommonsArgs } from 'nodecommons-es-cli';
import { commonsFileExists } from 'nodecommons-es-file';

import { EFaceApiModel, toEFaceApiModel } from './enums/eface-api-model';
import { EFaceDetectionSorting, toEFaceDetectionSorting } from './enums/eface-detection-sorting';

import { filterDetections } from './helpers/face-detection';

import { Jimp } from './classes/jimp';
import { FaceDetector } from './classes/face-detector';
import { GazeTrainer, TImageWithLandmarks } from './classes/gaze-trainer';
import { GazeDetector } from './classes/gaze-detector';

import { TDetection } from './types/tface-detection';
import { TLandmarks } from './types/tlandmarks';

const args: CommonsArgs = new CommonsArgs();
if (args.hasAttribute('debug')) commonsOutputSetDebugging(true);

if (args.hasAttribute('help') || !args.hasProperty('src') || !args.hasProperty('model')) {
	commonsOutputInfo(`Syntax:
faceapi-auto-crop --src="<source image>" --model=<model> [options]
node --experimental-modules --experimental-specifier-resolution=node dist/cli.js --src="<source image>" --model=<model> [options]

--help                         Show this screen
--debug                        Show debugging information
--model=<model>                ssdv1, tiny, etc.
--modelsPath=<path>            Path to the face-api.js models, e.g. ./models
--gazeModelPath=<path>         Path to the gaze detector models, e.g. ./gaze-model
--minConfidence=n              minConfidence for ssdv1 model
                               0.4 works well
--scoreThreshold=n             scoreThreshold for tiny model
--filter                       Apply filter to detections
--skipUpsideDown               Skip faces whose landmarks indicate they are upside down
--minWidthRatio                The minimum width ratio for detected faces. Default is 0.02 (2%)
--minHeightRatio               The minimum height ratio for detected faces. Default is 0.02 (2%)
--maxWidthRatio                The maximum width ratio for detected faces. Default is 0.8 (80%)
--maxHeightRatio               The maximum height ratio for detected faces. Default is 0.9 (90%)
--sorting=method               The detection sort method. Default is hybrid:
                               "score" to sort by detection score
                               "size" to sort by detection area size (bigger is better)
                               "hybrid" to use the hybrid approach:
                               First pivot on the score threshold, then sort each above and below
--highScoreThreshold           The threshold for the hybrid sort. Default is 0.9 (90%)
--maxDetections=n              The maximum number of detections (ordered by the sort quality from high to low). Default is 5
--detect                       Display detections and quit
--dest="<destination image>"
--width=pixels                 Width of the destination image
--height=pixels                Height of the destination image, or "ratio" for the ratio of the source image, or "detection" for the ratio of the detection area
--fullWidth                    Force to use the full width of the source image
                               This means that the crop will only shift the Y co-ordinate up and down
--shuntY=n                     Ratio of detected face height to shunt the Y co-ordinate down by
                               This is useful for portrait style cropping
                               0.2 works well
--gazeOffsetX                  Ratio of detected face width to shunt the X co-ordinate left or right
                               Direction is determined by detected gaze direction
                               This is useful for portrait style cropping, where people look to the centre of the image
                               0.8 works well
--minGrowthFaceWidthRatio=n    The minimum padded width based upon detected face widths. Default is 2 (200%)
                               This is a multiplication ratio for the total width, not left and right each
--minGrowthFaceHeightRatio=n   The minimum padded height upon detected face heights. Default is 1.5 (150%)
--minGrowthImageWidthRatio=n   The minimum padded width based upon the source image width. Default is 0.1 (10%)
--minGrowthImageHeightRatio=n  The minimum padded height based upon the source image height. Default is 0.1 (10%)
--drawOutlines                 Draw debugging outlines on destination image
--sharpen=n                    Sharpen the resulting image by n
                               0.1 works well

`);
	process.exit(1);
}

const DRAW_OUTLINES: boolean = args.hasAttribute('drawOutlines');

const srcFile: string = args.getString('src');
if (!commonsFileExists(srcFile)) commonsOutputDie('No such file exists');

const jimp: Jimp = new Jimp();

const modelName: string = args.getString('model');
const model: EFaceApiModel|undefined = toEFaceApiModel(modelName);
if (!model) commonsOutputDie('No such model available');
commonsOutputDebug(`Using model ${model}`);

const modelsPath: string|undefined = args.getStringOrUndefined('modelsPath');
const gazeModelPath: string|undefined = args.getStringOrUndefined('gazeModelPath');

type TFilterOptions = {
		sorting: EFaceDetectionSorting;
		highScoreThreshold: number;
		minWidthRatio: number;
		minHeightRatio: number;
		maxWidthRatio: number;
		maxHeightRatio: number;
		skipUpsideDown: boolean;
		maxDetections: number;
};

let filter: TFilterOptions|undefined;

if (args.hasAttribute('filter')) {
	const sortingName: string|undefined = args.getStringOrUndefined('sorting');
	let sorting: EFaceDetectionSorting|undefined;
	if (sortingName) {
		sorting = toEFaceDetectionSorting(sortingName);
		if (!sorting) commonsOutputDie('No such sorting approach exists');
	}

	filter = {
			sorting: sorting || EFaceDetectionSorting.HYBRID,
			highScoreThreshold: args.getNumberOrUndefined('highScoreThreshold') || 0.9,
			minWidthRatio: args.getNumberOrUndefined('minWidthRatio') || 0.02,
			minHeightRatio: args.getNumberOrUndefined('minHeightRatio') || 0.02,
			maxWidthRatio: args.getNumberOrUndefined('maxWidthRatio') || 0.8,
			maxHeightRatio: args.getNumberOrUndefined('maxHeightRatio') || 0.9,
			skipUpsideDown: args.hasAttribute('skipUpsideDown') || true,
			maxDetections: args.getNumberOrUndefined('maxDetections') || 5
	};

	commonsOutputDebug(`Using sorting method ${filter.sorting}`);
	if (filter.sorting === EFaceDetectionSorting.HYBRID) commonsOutputDebug(`Using highScoreThreshold ${filter.highScoreThreshold}`);
	commonsOutputDebug(`Using minWidthRatio ${filter.minWidthRatio}`);
	commonsOutputDebug(`Using minHeightRatio ${filter.minHeightRatio}`);
	commonsOutputDebug(`Using maxWidthRatio ${filter.maxWidthRatio}`);
	commonsOutputDebug(`Using maxHeightRatio ${filter.maxHeightRatio}`);
	if (filter.skipUpsideDown) commonsOutputDebug('Skipping upside down face detections');
	commonsOutputDebug(`Using maxDetections ${filter.maxDetections}`);
}

let options: faceapi.FaceDetectionOptions|undefined;

switch (model) {
	case EFaceApiModel.SSDV1: {
		const minConfidence: number|undefined = args.getNumberOrUndefined('minConfidence');
		
		if (minConfidence) {
			commonsOutputDebug(`Using minConfidence ${minConfidence}`);
			options = new faceapi.SsdMobilenetv1Options({
					minConfidence: minConfidence
			});
		}
		break;
	}
	case EFaceApiModel.TINY: {
		const scoreThreshold: number|undefined = args.getNumberOrUndefined('scoreThreshold');
		
		if (scoreThreshold) {
			commonsOutputDebug(`Using scoreThreshold ${scoreThreshold}`);
			options = new faceapi.TinyFaceDetectorOptions({
					scoreThreshold: scoreThreshold
			});
		}
		break;
	}
}

const faceDetector: FaceDetector = new FaceDetector(
		model,
		options
);

const gazeDetector: GazeDetector = new GazeDetector();

void (async (): Promise<void> => {
	commonsOutputDebug('Initialising face detector');
	await faceDetector.init(modelsPath);

	commonsOutputDebug('Initialising gaze detector');
	await gazeDetector.init(gazeModelPath);

	commonsOutputDebug(`Loading image from ${srcFile}`);

	await jimp.loadFromFile(srcFile);

	const buffer: Buffer = await jimp.getRawJimpImage().getBufferAsync('image/jpeg');

	commonsOutputDebug('Running detection');
	let detections: TDetection[] = await faceDetector.detect(buffer);

	if (filter) {
		detections = filterDetections(
				detections,
				filter.sorting,
				filter.highScoreThreshold,
				jimp.getWidth(),
				jimp.getHeight(),
				filter.minWidthRatio,
				filter.minHeightRatio,
				filter.maxWidthRatio,
				filter.maxHeightRatio,
				filter.skipUpsideDown,
				filter.maxDetections
		);
		commonsOutputDebug(`${detections.length} detections after filtering`);
	}

	if (args.hasAttribute('x-gaze-train')) {
		const outPath: string = args.getString('outPath');

		commonsOutputDebug('Running gaze training');
		
		const trainer: GazeTrainer = new GazeTrainer(jimp, detections);
		const extracted: TImageWithLandmarks[] = await trainer.extract();

		for (const image of extracted) {
			const uid: string = commonsBase62GenerateRandomId();

			await image.jimp.save(`${outPath}/${uid}_${image.leftEye.x},${image.leftEye.y}!${image.rightEye.x},${image.rightEye.y}!${image.nose.x},${image.nose.y}!${image.mouth.x},${image.mouth.y}.jpg`);
		}
		process.exit(1);
	}

	if (args.hasAttribute('detect')) {
		for (const detection of detections) {
			console.log(`Detection at ${detection.x},${detection.y} ${detection.width}x${detection.height} with score ${detection.score}`);
		}
		process.exit(1);
	}

	if (DRAW_OUTLINES && detections.length > 0) {
		commonsOutputDebug('Drawing outlines for detections');

		const first: TDetection = detections.shift()!;
		await jimp.drawDetectionBoundary(first, 0xff2000ff);

		for (const detection of detections) {
			await jimp.drawDetectionBoundary(detection, 0x0080ffff);
		}

		detections.unshift(first);

		for (const detection of detections) {
			if (detection.landmarks.leftEye) {
				await jimp.drawDetectionBoundary(
						{
								x: detection.landmarks.leftEye.x - 5,
								y: detection.landmarks.leftEye.y - 5,
								width: 10,
								height: 10
						},
						0xffffffff
				);
			}
			if (detection.landmarks.rightEye) {
				await jimp.drawDetectionBoundary(
						{
								x: detection.landmarks.rightEye.x - 5,
								y: detection.landmarks.rightEye.y - 5,
								width: 10,
								height: 10
						},
						0xffa000ff
				);
			}
			if (detection.landmarks.nose) {
				await jimp.drawDetectionBoundary(
						{
								x: detection.landmarks.nose.x - 5,
								y: detection.landmarks.nose.y - 5,
								width: 10,
								height: 10
						},
						0xa0ff00ff
				);
			}
			if (detection.landmarks.mouth) {
				await jimp.drawDetectionBoundary(
						{
								x: detection.landmarks.mouth.x - 5,
								y: detection.landmarks.mouth.y - 5,
								width: 10,
								height: 10
						},
						0xa0ffffff
				);
			}
		}
	}

	const shuntY: number|undefined = args.getNumberOrUndefined('shuntY');
	if (shuntY) {
		commonsOutputDebug(`Shunting all detections down by ${shuntY}. This is useful since faces usually prefer portait positioning rather than literal centre`);
		for (const detection of detections) {
			detection.y += detection.height * shuntY;
			commonsOutputDebug(`Shunted detection is now ${detection.x},${detection.y} ${detection.width}x${detection.height}`);
		}
	}

	const gazeOffsetX: number|undefined = args.getNumberOrUndefined('gazeOffsetX');
	if (gazeOffsetX) {
		commonsOutputDebug(`Shunting all detections by offset ${gazeOffsetX} left/right for detected gaze direction. This is useful since faces usually prefer padding towards where they are looking`);
		for (const detection of detections) {
			if (!detection.landmarks.leftEye || !detection.landmarks.rightEye || !detection.landmarks.nose || !detection.landmarks.mouth) continue;

			const relative: TLandmarks = {
					leftEye: {
							x: (detection.landmarks.leftEye.x - detection.x) / detection.width,
							y: (detection.landmarks.leftEye.y - detection.y) / detection.height
					},
					rightEye: {
							x: (detection.landmarks.rightEye.x - detection.x) / detection.width,
							y: (detection.landmarks.rightEye.y - detection.y) / detection.height
					},
					nose: {
							x: (detection.landmarks.nose.x - detection.x) / detection.width,
							y: (detection.landmarks.nose.y - detection.y) / detection.height
					},
					mouth: {
							x: (detection.landmarks.mouth.x - detection.x) / detection.width,
							y: (detection.landmarks.mouth.y - detection.y) / detection.height
					}
			};

			const xy: TXy = gazeDetector.predict(relative);

			const delta: number = detection.width * gazeOffsetX * xy.x;

			if (DRAW_OUTLINES) {
				const x1: number = Math.floor(detection.x + (detection.width / 2));
				const x2: number = Math.floor(x1 + delta);

				const xMin: number = Math.min(x1, x2);
				const xMax: number = Math.max(x1, x2);
				const w: number = xMax - xMin;

				await jimp.fillArea(
						xMin,
						Math.floor(detection.y + (detection.height / 2)),
						w,
						5,
						0x000000ff
				);
			}

			detection.x += detection.width * gazeOffsetX * xy.x;
			commonsOutputDebug(`Shunted detection is now ${detection.x},${detection.y} ${detection.width}x${detection.height}`);
		}
	}

	if (DRAW_OUTLINES && detections.length > 0 && (shuntY || gazeOffsetX)) {
		commonsOutputDebug('Drawing shunted outlines for detections');

		for (const detection of detections) {
			await jimp.drawDetectionBoundary(detection, 0xffff00ff);
		}
	}

	const destWidth: number|undefined = args.getNumberOrUndefined('width');
	const destHeightString: string|undefined = args.getStringOrUndefined('height');
	if ((destWidth && !destHeightString) || (!destWidth && destHeightString)) commonsOutputDie('Both the width and height need to be specified if one is. "ratio" and "detection" are alternatives for fluid heights.');
	if (destWidth && destHeightString) {
		let destHeight: number|'detection'|undefined;

		switch (destHeightString) {
			case 'ratio': {
				commonsOutputDebug('Ratio height specified, so using ratio from source image');
				const ratio: number = jimp.getWidth() / jimp.getHeight();
				destHeight = Math.max(1, Math.round(destWidth / ratio));
				break;
			}
			case 'detection': {
				commonsOutputDebug('Detection height specified, so will use ratio from detection area');
				destHeight = 'detection';
				break;
			}
			default:
				destHeight = commonsTypeAttemptNumber(destHeightString);
				break;
		}
		if (!destHeight) commonsOutputDie('Invalid height supplied');

		commonsOutputDebug(`Cropping and resizing image to destination dimensions ${destWidth}x${destHeight}`);

		let minGrowthFaceWidthRatio: number|undefined = args.getNumberOrUndefined('minGrowthFaceWidthRatio');
		if (minGrowthFaceWidthRatio === undefined) minGrowthFaceWidthRatio = 2;
		if (minGrowthFaceWidthRatio < 0) commonsOutputDie('minGrowthFaceWidthRatio cannot be negative');

		let minGrowthFaceHeightRatio: number|undefined = args.getNumberOrUndefined('minGrowthFaceHeightRatio');
		if (minGrowthFaceHeightRatio === undefined) minGrowthFaceHeightRatio = 1.5;
		if (minGrowthFaceHeightRatio < 0) commonsOutputDie('minGrowthFaceHeightRatio cannot be negative');

		let minGrowthImageWidthRatio: number|undefined = args.getNumberOrUndefined('minGrowthImageWidthRatio');
		if (minGrowthImageWidthRatio === undefined) minGrowthImageWidthRatio = 0.1;
		if (minGrowthImageWidthRatio < 0) commonsOutputDie('minGrowthImageWidthRatio cannot be negative');

		let minGrowthImageHeightRatio: number|undefined = args.getNumberOrUndefined('minGrowthImageHeightRatio');
		if (minGrowthImageHeightRatio === undefined) minGrowthImageHeightRatio = 0.1;
		if (minGrowthImageHeightRatio < 0) commonsOutputDie('minGrowthImageHeightRatio cannot be negative');
		
		commonsOutputDebug(`Using minimum growth faceRatio ${minGrowthFaceWidthRatio}x${minGrowthFaceHeightRatio} and imageRatio ${minGrowthImageWidthRatio}x${minGrowthImageHeightRatio}`);

		await jimp.cropAndResize(
				detections,
				destWidth,
				destHeight,
				{ width: minGrowthFaceWidthRatio, height: minGrowthFaceHeightRatio },
				{ width: minGrowthImageWidthRatio, height: minGrowthImageHeightRatio },
				args.hasProperty('fullWidth'),
				DRAW_OUTLINES
		);
	}

	const sharpen: number|undefined = args.getNumberOrUndefined('sharpen');
	if (sharpen) {
		commonsOutputDebug(`Sharpening image by ${sharpen}`);
		await jimp.sharpen(sharpen);
	}

	const quality: number|undefined = args.getNumberOrUndefined('quality');
	if (quality) {
		await jimp.quality(quality);
	}

	const destFile: string = args.getString('dest');

	commonsOutputDebug(`Saving to ${destFile}`);
	await jimp.save(destFile);
})();
