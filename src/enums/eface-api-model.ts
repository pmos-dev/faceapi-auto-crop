import { commonsTypeIsString } from 'tscommons-es-core';

export enum EFaceApiModel {
		SSDV1 = 'ssdv1',
		TINY = 'tiny',
		MTCNN = 'mtcnn'
}

export function toEFaceApiModel(type: string): EFaceApiModel|undefined {
	switch (type) {
		case EFaceApiModel.SSDV1.toString():
			return EFaceApiModel.SSDV1;
		case EFaceApiModel.TINY.toString():
			return EFaceApiModel.TINY;
		case EFaceApiModel.MTCNN.toString():
			return EFaceApiModel.MTCNN;
	}
	return undefined;
}

export function fromEFaceApiModel(type: EFaceApiModel): string {
	switch (type) {
		case EFaceApiModel.SSDV1:
			return EFaceApiModel.SSDV1.toString();
		case EFaceApiModel.TINY:
			return EFaceApiModel.TINY.toString();
		case EFaceApiModel.MTCNN:
			return EFaceApiModel.MTCNN.toString();
	}
	
	throw new Error('Unknown EFaceApiModel');
}

export function isEFaceApiModel(test: unknown): test is EFaceApiModel {
	if (!commonsTypeIsString(test)) return false;
	
	return toEFaceApiModel(test) !== undefined;
}

export function keyToEFaceApiModel(key: string): EFaceApiModel {
	switch (key) {
		case 'SSDV1':
			return EFaceApiModel.SSDV1;
		case 'TINY':
			return EFaceApiModel.TINY;
		case 'MTCNN':
			return EFaceApiModel.MTCNN;
	}
	
	throw new Error(`Unable to obtain EFaceApiModel for key: ${key}`);
}

export const EFACE_API_MODELS: EFaceApiModel[] = Object.keys(EFaceApiModel)
		.map((e: string): EFaceApiModel => keyToEFaceApiModel(e));
