import { commonsTypeIsString } from 'tscommons-es-core';

export enum EFaceDetectionSorting {
		SCORE = 'score',
		SIZE = 'size',
		HYBRID = 'hybrid'
}

export function toEFaceDetectionSorting(type: string): EFaceDetectionSorting|undefined {
	switch (type) {
		case EFaceDetectionSorting.SCORE.toString():
			return EFaceDetectionSorting.SCORE;
		case EFaceDetectionSorting.SIZE.toString():
			return EFaceDetectionSorting.SIZE;
		case EFaceDetectionSorting.HYBRID.toString():
			return EFaceDetectionSorting.HYBRID;
	}
	return undefined;
}

export function fromEFaceDetectionSorting(type: EFaceDetectionSorting): string {
	switch (type) {
		case EFaceDetectionSorting.SCORE:
			return EFaceDetectionSorting.SCORE.toString();
		case EFaceDetectionSorting.SIZE:
			return EFaceDetectionSorting.SIZE.toString();
		case EFaceDetectionSorting.HYBRID:
			return EFaceDetectionSorting.HYBRID.toString();
	}
	
	throw new Error('Unknown EFaceDetectionSorting');
}

export function isEFaceDetectionSorting(test: unknown): test is EFaceDetectionSorting {
	if (!commonsTypeIsString(test)) return false;
	
	return toEFaceDetectionSorting(test) !== undefined;
}

export function keyToEFaceDetectionSorting(key: string): EFaceDetectionSorting {
	switch (key) {
		case 'SCORE':
			return EFaceDetectionSorting.SCORE;
		case 'SIZE':
			return EFaceDetectionSorting.SIZE;
		case 'HYBRID':
			return EFaceDetectionSorting.HYBRID;
	}
	
	throw new Error(`Unable to obtain EFaceDetectionSorting for key: ${key}`);
}

export const EFACE_DETECTION_SORTINGS: EFaceDetectionSorting[] = Object.keys(EFaceDetectionSorting)
		.map((e: string): EFaceDetectionSorting => keyToEFaceDetectionSorting(e));
