import { commonsNumberAverage } from 'tscommons-es-core';
import { IDimensions } from 'tscommons-es-graphics';

import { commonsOutputDebug } from 'nodecommons-es-cli';

import { TDetection } from '../types/tface-detection';

import { EFaceDetectionSorting } from '../enums/eface-detection-sorting';

type THighLowDetections = {
		high: TDetection[];
		low: TDetection[];
};

function splitDetectionsByScore(
		detections: TDetection[],
		threshold: number
): THighLowDetections {
	return {
			high: detections
					.filter((detection: TDetection): boolean => detection.score >= threshold),
			low: detections
					.filter((detection: TDetection): boolean => detection.score < threshold)
	};
}

function sortDetectionsBySize(detections: TDetection[]): TDetection[] {
	return [ ...detections ]
			.sort((a: TDetection, b: TDetection): number => {
				const aSize: number = a.width * a.height;
				const bSize: number = b.width * b.height;

				if (aSize < bSize) return -1;
				if (aSize > bSize) return 1;

				return 0;
			})
			.reverse();
}

function filterDetectionsSize(
		detections: TDetection[],
		imageWidth: number,
		imageHeight: number,
		minWidthRatio: number,
		minHeightRatio: number,
		maxWidthRatio: number = 1,
		maxHeightRatio: number = 1
): TDetection[] {
	type TWithRatio = {
			detection: TDetection;
			widthRatio: number;
			heightRatio: number;
	}
	return detections
			.map((detection: TDetection): TWithRatio => ({
					detection: detection,
					widthRatio: detection.width / imageWidth,
					heightRatio: detection.height / imageHeight
			}))
			.filter((detection: TWithRatio): boolean => detection.widthRatio >= minWidthRatio
					&& detection.heightRatio >= minHeightRatio
					&& detection.widthRatio < maxWidthRatio
					&& detection.heightRatio < maxHeightRatio
			)
			.map((detection: TWithRatio): TDetection => detection.detection);
}

export function filterDetections(
		detections: TDetection[],
		sorting: EFaceDetectionSorting,
		splitScoreThreshold: number,
		imageWidth: number,
		imageHeight: number,
		minWidthRatio: number,
		minHeightRatio: number,
		maxWidthRatio: number = 1,
		maxHeightRatio: number = 1,
		skipUpsideDown?: boolean,
		maxDetections?: number
): TDetection[] {
	detections = [ ...detections ];

	if (skipUpsideDown) {
		detections = detections
				.filter((detection: TDetection): boolean => {
					if (!detection.landmarks.nose && !detection.landmarks.mouth) return true;	// can't check without a nose or mouth

					const eyeYs: number[] = [];
					if (detection.landmarks.leftEye) eyeYs.push(detection.landmarks.leftEye.y);
					if (detection.landmarks.rightEye) eyeYs.push(detection.landmarks.rightEye.y);
					if (eyeYs.length === 0) return true;	// can't check without an eye

					const eyeY: number = commonsNumberAverage(eyeYs)!;

					// try mouth first, as it is further away
					if (detection.landmarks.mouth) {
						if (detection.landmarks.mouth.y < eyeY) return false;
						return true;
					}

					if (detection.landmarks.nose) {
						if (detection.landmarks.nose.y < eyeY) return false;
						return true;
					}

					return true;
				});
	}

	const sized: TDetection[] = filterDetectionsSize(
			detections,
			imageWidth,
			imageHeight,
			minWidthRatio,
			minHeightRatio,
			maxWidthRatio,
			maxHeightRatio
	);
	commonsOutputDebug(`${sized.length} detections after size filtering`);

	let sorted: TDetection[] = sized;

	switch (sorting) {
		case EFaceDetectionSorting.HYBRID:
			const highLow: THighLowDetections = splitDetectionsByScore(
					sized,
					splitScoreThreshold
			);
			commonsOutputDebug(`${highLow.high.length} high score detections; ${highLow.low.length} low score detections`);

			const combined: TDetection[] = [
					...sortDetectionsBySize(highLow.high),
					...sortDetectionsBySize(highLow.low)
			];
			commonsOutputDebug(`${combined.length} total detections available`);

			sorted = combined;
			break;
		case EFaceDetectionSorting.SCORE:
			sorted = sized
					.sort((a: TDetection, b: TDetection): number => {
						if (a.score < b.score) return -1;
						if (a.score > b.score) return 1;
						
						return 0;
					})
					.reverse();
			break;
		case EFaceDetectionSorting.SIZE:
			sorted = sortDetectionsBySize(sized);
			break;
	}

	return sorted
			.slice(0, maxDetections);
}

export function getCombinedBoundary(detections: IDimensions[]): IDimensions {
	if (detections.length === 0) throw new Error('No detections to make combined boundary from');

	const minX: number = Math.min(...detections.map((detection: TDetection): number => detection.x));
	const maxX: number = Math.max(...detections.map((detection: TDetection): number => (detection.x + detection.width)));
	const minY: number = Math.min(...detections.map((detection: TDetection): number => detection.y));
	const maxY: number = Math.max(...detections.map((detection: TDetection): number => (detection.y + detection.height)));

	return {
			x: minX,
			y: minY,
			width: maxX - minX,
			height: maxY - minY
	};
}
