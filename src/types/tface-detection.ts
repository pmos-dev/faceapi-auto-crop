import { IDimensions } from 'tscommons-es-graphics';

import { TLandmarks } from './tlandmarks';

export type TDetection = IDimensions & {
		score: number;
		landmarks: TLandmarks;
};
