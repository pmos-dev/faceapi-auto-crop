import { TXy } from 'tscommons-es-graphics';

export type TLandmarks = {
		leftEye: TXy|undefined;
		rightEye: TXy|undefined;
		nose: TXy|undefined;
		mouth: TXy|undefined;
};
